package Massiv;

import java.sql.Array;
import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите длину массива:");
        int n = in.nextInt();

        int a[] = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }

        System.out.println("Массив:");
        System.out.print(Arrays.toString(a));

        System.out.println();

        double b[] = new double[n];

        for (int i = 0; i < n; i++) {
            b[i] = a[i] * 1.1;
        }
        System.out.println();

        System.out.println("Массив после увеличения чисел на 10%:");
        System.out.println(Arrays.toString(b));

        boolean sortirovka = false;
        while (!sortirovka) {
            sortirovka = true;
            for (int i = 0; i < n - 1; ++i) {
                if (b[i] < b[i + 1]) {
                    sortirovka = false;
                    double k = b[i];
                    b[i] = b[i + 1];
                    b[i + 1] = k;
                }
            }
        }

        System.out.println();

        System.out.println("Пузырек:");
        System.out.println(Arrays.toString(b));
    }
}
